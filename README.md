# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ: сменить контекст вызова можно с помощью методов bind, call, apply. Метод bind позволяет "привязать" контекст к определенной функции, а вызвать ее можно потом. Методы call и apply сразу вызывают функцию с нужным контекстом, указав его как аргумент метода. После контекста, через запятую, передаются аргументы для функции. В call аргументы передаются через запятую, а в apple в виде массива.
#### 2. Что такое стрелочная функция?
> Ответ: стрелочная функция - это новый стандарт записи функций в языке JS. Основные отличия от стандартной записи функций: более короткая запись и отсутствие своего this.
#### 3. Приведите свой пример конструктора. 
```js
  function Phone(manufacturer, model, color) {
    this.manufacturer = manufacturer;
    this.model = model;
    this.color = color;
  }
  const iphone7 = new Phone('Apple', 'iPhone 7', 'Space Grey');
```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
  // метод bind()
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }.bind(this), 1000)
    }
  }

  // сохранение ссылки на this в переменной
  const person = {
    name: 'Nikita',
    sayHello: function() {
      const that = this;
      setTimeout(function() {
          console.log(that.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  // использование стрелочной функции
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(() => console.log(this.name + ' says hello to everyone!'), 1000)
    }
  }
```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
